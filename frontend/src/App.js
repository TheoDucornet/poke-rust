import { useForkRef } from "@mui/material";
import PokemonInput from "./components/PokemonInput";
import TableauPokemon from "./components/TableauPokemon";
import React, { useState, useEffect } from "react";

const App = () => {
	const [pokemons, setPokemons] = useState([]);

	const loadPokemons = () => {
		fetch(`http://localhost:3000/api/v1/pokemon`)
			.then((response) => response.json())
			.then((data) => {
				setPokemons(
					data.map((pokemon) => {
						const moves = [];
						if (pokemon.move1) moves.push(pokemon.move1);
						if (pokemon.move2) moves.push(pokemon.move2);
						if (pokemon.move3) moves.push(pokemon.move3);
						if (pokemon.move4) moves.push(pokemon.move4);
						return {
							id: pokemon.id,
							name: pokemon.name,
							ability: pokemon.ability,
							item: pokemon.item,
							image: pokemon.lien_image,
							moves: moves,
						};
					})
				);
			});
	};

	useEffect(loadPokemons, []);

	const handleDelete = (id) => {
		fetch(`http://localhost:3000/api/v1/pokemon/${id}`, {
			method: "DELETE",
			headers: {
				"Content-Type": "application/json",
			},
		}).then((response) => {
			if (response.ok)
				setPokemons(pokemons.filter((pokemon) => pokemon.id !== id));
		});
	};

	return (
		<>
			<PokemonInput onSubmit={loadPokemons} />
			<TableauPokemon pokemons={pokemons} onDelete={handleDelete} />
		</>
	);
};

export default App;
