import { Box, Grid, Typography } from "@mui/material";

import React, { useState, useEffect } from "react";
import PokemonThumbnail from "./PokemonThumbnail";

const TableauPokemon = ({ pokemons, onDelete }) => {
	return (
		<>
			<Typography
				variant="h4"
				component="h4"
				sx={{
					fontFamily: "monospace",
					fontWeight: 700,
					textDecoration: "none",
					marginTop: "20px",
					textAlign: "center",
				}}
			>
				POKÉMON AU PC
			</Typography>
			<Box sx={{ marginTop: "30px", marginBottom: "10px" }}>
				<Grid container spacing={2} justifyContent="center">
					{pokemons.map((pokemon) => (
						<Grid item xs={3} key={pokemon.id}>
							<PokemonThumbnail
								pokemon={pokemon}
								onDelete={onDelete}
							/>
						</Grid>
					))}
				</Grid>
			</Box>
		</>
	);
};

export default TableauPokemon;
