import React, { useEffect } from "react";
import {
	Card,
	CardMedia,
	CardHeader,
	CardContent,
	IconButton,
	Typography,
	Grid,
	Box,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";

const PokemonThumbnail = ({ pokemon, onDelete }) => {
	const [imageDimensions, setImageDimensions] = React.useState({
		width: 0,
		height: 0,
	});

	useEffect(() => {
		const img = new Image();
		img.onload = () => {
			setImageDimensions({
				width: img.width * 3,
				height: img.height * 3,
			});
		};
		img.src = pokemon.image;
	}, []);

	return (
		<Card sx={{ maxWidth: 345, margin: 2 }}>
			<CardHeader
				action={
					<IconButton
						aria-label="delete"
						onClick={() => onDelete(pokemon.id)}
					>
						<DeleteIcon />
					</IconButton>
				}
				title={pokemon.name}
				titleTypographyProps={{ variant: "h5" }}
			/>
			<Box
				sx={{
					display: "flex",
					justifyContent: "center",
					alignItems: "center",
					marginBottom: 2,
				}}
			>
				<CardMedia
					component="img"
					height="140"
					image={pokemon.image}
					alt={pokemon.name}
					sx={{
						marginBottom: 2,
						width: "100%",
						maxWidth: `${imageDimensions.width}px`,
						height: "auto",
					}}
				/>
			</Box>
			<CardContent>
				<Typography variant="body2" color="text.secondary">
					Talent: {pokemon.ability}
				</Typography>
				<Typography variant="body2" color="text.secondary">
					Attaques:
				</Typography>
				<ul>
					{pokemon.moves.map((move, index) => (
						<li key={index}>
							<Typography variant="body2" color="text.secondary">
								{move}
							</Typography>
						</li>
					))}
				</ul>
			</CardContent>
		</Card>
	);
};

export default PokemonThumbnail;
