import {
	Button,
	Box,
	Input,
	Typography,
	FormControl,
	CardMedia,
} from "@mui/material";
import React, { useState, useEffect, useRef } from "react";
import PokemonDico from "../dico/PokemonDico";
import AsyncSelect from "react-select/async";
import Select from "react-select";

const PokemonInput = ({ onSubmit }) => {
	const [pokemon, setPokemon] = useState({
		name: "",
		ability: "",
		item: "",
		moves: [],
	});
	const [error, setError] = useState("");
	const [abilities, setAbilities] = useState();
	const [moves, setMoves] = useState();
	const [img, setImg] = useState();
	const [imageDimensions, setImageDimensions] = React.useState({
		width: 0,
		height: 0,
	});
	const selectInputRef = useRef();

	useEffect(() => {
		if (pokemon.name !== "")
			fetch(`http://localhost:3000/api/v1/info/${pokemon.name}`)
				.then((response) => response.json())
				.then((data) => {
					setAbilities(
						data.abilities.map((ability) => ({
							value: ability,
							label: ability,
						}))
					);
					setMoves(
						data.moves.map((ability) => ({
							value: ability,
							label: ability,
						}))
					);
					setImg(data.sprite_url);
					const img = new Image();
					img.onload = () => {
						setImageDimensions({
							width: img.width,
							height: img.height,
						});
					};
					img.src = data.sprite_url;
				});
	}, [pokemon.name]);

	const buildBody = () => ({
		pokemon_id: Number(img.match(/\/(\d+)\.png$/)[1]),
		ability: pokemon.ability,
		item: pokemon.item,
		lien_image: img,
		move1: pokemon.moves[0] ? pokemon.moves[0] : "",
		move2: pokemon.moves[1] ? pokemon.moves[1] : "",
		move3: pokemon.moves[2] ? pokemon.moves[2] : "",
		move4: pokemon.moves[3] ? pokemon.moves[3] : "",
	});

	const handleSubmit = (event) => {
		event.preventDefault();
		fetch("http://localhost:3000/api/v1/pokemon/new", {
			method: "POST",
			body: JSON.stringify(buildBody()),
			headers: {
				"Content-Type": "application/json",
			},
		}).then((response) => {
			if (response.ok) clear();
		});
	};

	const clear = () => {
		onSubmit();
		setAbilities();
		setMoves();
		setImg();
		setPokemon({
			name: "",
			ability: "",
			item: "",
			moves: [],
		});
		selectInputRef.current.clearValue();
	};

	const handleChange = (key, value) => {
		setPokemon({ ...pokemon, [key]: value });
	};

	const loadPokemons = (inputValue, callback) => {
		fetch("http://localhost:3000/api/v1/pokedex")
			.then((response) => response.json())
			.then((data) => {
				callback(
					data
						.map((pokemon) => ({
							value: pokemon.nom_anglais,
							label: pokemon.nom_francais,
						}))
						.filter((pokemon) =>
							pokemon.label
								.toLowerCase()
								.includes(inputValue.toLowerCase())
						)
				);
			});
	};

	return (
		<Box
			component="form"
			onSubmit={handleSubmit}
			sx={{
				maxWidth: 345,
				margin: "auto",
				padding: 2,
				border: "1px solid #ccc",
				borderRadius: "4px",
				bgcolor: "#ffffff",
			}}
		>
			{error && (
				<Typography
					variant="body2"
					color="error"
					sx={{ textAlign: "center", marginTop: "10px" }}
				>
					{error}
				</Typography>
			)}
			<FormControl fullWidth sx={{ marginBottom: 2 }}>
				<Typography
					variant="body1"
					component="h5"
					sx={{ fontWeight: 700 }}
				>
					{PokemonDico.NAME}
				</Typography>
				<AsyncSelect
					ref={selectInputRef}
					loadOptions={loadPokemons}
					onChange={(selectedOption) => {
						if (selectedOption)
							handleChange("name", selectedOption.value);
					}}
					defaultOptions
				/>
			</FormControl>
			{img && (
				<Box
					sx={{
						display: "flex",
						justifyContent: "center",
						alignItems: "center",
						marginBottom: 2,
					}}
				>
					<CardMedia
						component="img"
						height="140"
						image={img}
						alt={pokemon.name}
						sx={{
							marginBottom: 2,
							width: "100%",
							maxWidth: `${imageDimensions.width}px`,
							height: "auto",
						}}
					/>
				</Box>
			)}
			<FormControl fullWidth sx={{ marginBottom: 2 }}>
				{abilities && (
					<>
						<Typography
							variant="body1"
							component="h5"
							sx={{ fontWeight: 700 }}
						>
							{PokemonDico.ABILITY}
						</Typography>
						<Select
							options={abilities}
							onChange={(selectedOption) =>
								handleChange("ability", selectedOption.value)
							}
						/>
					</>
				)}
			</FormControl>
			<FormControl fullWidth sx={{ marginBottom: 2 }}>
				{moves && (
					<>
						<Typography
							variant="body1"
							component="h5"
							sx={{ fontWeight: 700 }}
						>
							{PokemonDico.MOVES}
						</Typography>
						<Select
							options={moves}
							isMulti
							onChange={(selectedOptions) =>
								handleChange(
									"moves",
									selectedOptions.map(
										(option) => option.value
									)
								)
							}
							isOptionDisabled={() => pokemon.moves.length >= 4}
						/>
					</>
				)}
			</FormControl>
			<Box
				sx={{
					display: "flex",
					justifyContent: "center",
					alignItems: "center",
					marginBottom: 2,
				}}
			>
				<Button type="submit" variant="contained" color="primary">
					{PokemonDico.SUBMIT}
				</Button>
			</Box>
		</Box>
	);
};

export default PokemonInput;
