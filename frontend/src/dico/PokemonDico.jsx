export default {
	NAME: "Pokémon",
	MOVES: "Moves",
	ABILITY: "Ability",
	ITEM: "Item",
	SUBMIT: "Create",
};
