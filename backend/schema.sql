DROP TABLE IF EXISTS pokemon;
DROP TABLE IF EXISTS pokedex;

CREATE TABLE pokedex
(
    id      INTEGER GENERATED ALWAYS AS IDENTITY,
    nom_Anglais VARCHAR(255) NOT NULL,
    nom_Francais VARCHAR(255) NOT NULL,
    CONSTRAINT PK_pokedex_id PRIMARY KEY (id)
);

CREATE TABLE pokemon
(
    id      INTEGER GENERATED ALWAYS AS IDENTITY,
    pokemon_id    INTEGER REFERENCES pokedex (id),
    ability VARCHAR(255) NULL,
    move1   VARCHAR(255) NULL,
    move2   VARCHAR(255) NULL,
    move3   VARCHAR(255) NULL,
    move4   VARCHAR(255) NULL,
    lien_image   VARCHAR(255) NULL,
    CONSTRAINT PK_pokemon_id PRIMARY KEY (id)
);

INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('bulbasaur', 'bulbizarre');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('ivysaur', 'herbizarre');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('venusaur', 'florizarre');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('charmander', 'salameche');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('charmeleon', 'reptincel');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('charizard', 'dracaufeu');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('squirtle', 'carapuce');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('wartortle', 'carabaffe');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('blastoise', 'tortank');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('caterpie', 'chenipan');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('metapod', 'chrysacier');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('butterfree', 'papilusion');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('weedle', 'aspicot');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('kakuna', 'coconfort');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('beedrill', 'dardargnan');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('pidgey', 'roucool');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('pidgeotto', 'roucoups');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('pidgeot', 'roucarnage');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('rattata', 'rattata');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('raticate', 'rattatac');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('spearow', 'piafabec');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('fearow', 'rapasdepic');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('ekans', 'abo');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('arbok', 'arbok');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('pikachu', 'pikachu');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('raichu', 'raichu');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('sandshrew', 'sabelette');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('sandslash', 'sablaireau');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('nidoran♀', 'nidoran♀');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('nidorina', 'nidorina');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('nidoqueen', 'nidoqueen');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('nidoran♂', 'nidoran♂');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('nidorino', 'nidorino');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('nidoking', 'nidoking');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('clefairy', 'melofee');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('clefable', 'melodelfe');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('vulpix', 'goupix');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('ninetales', 'feunard');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('jigglypuff', 'rondoudou');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('wigglytuff', 'grodoudou');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('zubat', 'nosferapti');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('golbat', 'nosferalto');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('oddish', 'mystherbe');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('gloom', 'ortide');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('vileplume', 'rafflesia');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('paras', 'paras');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('parasect', 'parasect');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('venonat', 'mimitoss');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('venomoth', 'aeromite');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('diglett', 'taupiqueur');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('dugtrio', 'triopikeur');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('meowth', 'miaouss');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('persian', 'persian');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('psyduck', 'psykokwak');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('golduck', 'akwakwak');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('mankey', 'ferosinge');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('primeape', 'colossinge');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('growlithe', 'caninos');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('arcanine', 'arcanin');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('poliwag', 'ptitard');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('poliwhirl', 'tetarte');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('poliwrath', 'tartard');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('abra', 'abra');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('kadabra', 'kadabra');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('alakazam', 'alakazam');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('machop', 'machoc');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('machoke', 'machopeur');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('machamp', 'mackogneur');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('bellsprout', 'chetiflor');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('weepinbell', 'boustiflor');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('victreebel', 'empiflor');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('tentacool', 'tentacool');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('tentacruel', 'tentacruel');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('geodude', 'racaillou');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('graveler', 'gravalanch');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('golem', 'grolem');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('ponyta', 'ponyta');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('rapidash', 'galopa');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('slowpoke', 'ramoloss');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('slowbro', 'flagadoss');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('magnemite', 'magneti');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('magneton', 'magneton');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('farfetchd', 'canarticho');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('doduo', 'doduo');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('dodrio', 'dodrio');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('seel', 'otaria');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('dewgong', 'lamantine');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('grimer', 'tadmorv');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('muk', 'grotadmorv');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('shellder', 'kokiyas');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('cloyster', 'crustabri');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('gastly', 'fantominus');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('haunter', 'spectrum');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('gengar', 'ectoplasma');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('onix', 'onix');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('drowzee', 'soporifik');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('hypno', 'hypnomade');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('krabby', 'krabby');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('kingler', 'krabboss');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('voltorb', 'voltorbe');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('electrode', 'electrode');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('exeggcute', 'noeunoeuf');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('exeggutor', 'noadkoko');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('cubone', 'osselait');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('marowak', 'ossatueur');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('hitmonlee', 'kicklee');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('hitmonchan', 'tygnon');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('lickitung', 'excelangue');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('koffing', 'smogo');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('weezing', 'smogogo');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('rhyhorn', 'rhinocorne');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('rhydon', 'rhinoféros');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('chansey', 'leveinard');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('tangela', 'saquedeneu');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('kangaskhan', 'kangourex');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('horsea', 'hypotrempe');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('seadra', 'hypocean');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('goldeen', 'poissirene');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('seaking', 'poissoroy');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('staryu', 'stari');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('starmie', 'staross');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('mr. mime', 'm.mime');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('scyther', 'insecateur');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('jynx', 'lippoutou');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('electabuzz', 'electek');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('magmar', 'magmar');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('pinsir', 'scarabrute');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('tauros', 'tauros');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('magikarp', 'magicarpe');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('gyarados', 'leviator');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('lapras', 'lokhlass');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('ditto', 'metamorph');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('eevee', 'evoli');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('vaporeon', 'aquali');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('jolteon', 'voltali');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('flareon', 'pyroli');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('porygon', 'porygon');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('omanyte', 'amonita');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('omastar', 'amonistar');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('kabuto', 'kabuto');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('kabutops', 'kabutops');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('aerodactyl', 'ptera');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('snorlax', 'ronflex');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('articuno', 'artikodin');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('zapdos', 'electhor');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('moltres', 'sulfura');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('dratini', 'minidraco');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('dragonair', 'draco');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('dragonite', 'dracolosse');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('mewtwo', 'mewtwo');
INSERT INTO pokedex (nom_Anglais, nom_Francais) VALUES ('mew', 'mew');


INSERT INTO pokemon(pokemon_id, ability, move1, move2, move3, move4, lien_image) VALUES (25, 'Static', 'Thunder Shock', 'Growl', 'Quick Attack', 'Electro Ball', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/25.png');
INSERT INTO pokemon(pokemon_id, ability, move1, move2, move3, move4, lien_image) VALUES (1, 'Overgrow', 'Tackle', 'Growl', 'Vine Whip', 'Poison Powder', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png');


