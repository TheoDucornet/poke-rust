use axum::extract::{State, Path};
use axum::http::StatusCode;
use axum::response::IntoResponse;
use sea_orm::EntityTrait;
use crate::AppState;
use crate::models::Pokemon;

pub async fn delete_pokemon(State(state): State<AppState>, Path(id): Path<i32>) -> impl IntoResponse {
    let res = Pokemon::delete_by_id(id).exec(&state.db).await.unwrap();

    if res.rows_affected > 0 {
        (StatusCode::OK, "Pokemon deleted successfully").into_response()
    } else {
        (StatusCode::NOT_FOUND, "Pokemon not found").into_response()
    }
}