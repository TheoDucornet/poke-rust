use axum::extract::State;
use axum::http::StatusCode;
use axum::Json;
use axum::response::IntoResponse;
use sea_orm::ActiveModelTrait;
use serde_json::json;
use crate::AppState;
use crate::models::{PokemonCreate, PokemonActiveModel};

pub async fn create_new_pokemon(State(state): State<AppState>, Json(new_pokemon): Json<PokemonCreate>) -> impl IntoResponse {
    let pokemon = PokemonActiveModel::from_json(json!(new_pokemon));

    pokemon.unwrap().insert(&state.db).await.unwrap();

    (StatusCode::CREATED, "Pokémon created successfully").into_response()
}