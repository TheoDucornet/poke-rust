use axum::http::StatusCode;
use axum::{Json, extract::Path};
use axum::extract::State;
use axum::response::IntoResponse;
use reqwest;
use crate::models::{PokemonExternalInfo, PokemonInfo, PokemonMapper, Pokedex, Pokemon};
use sea_orm::EntityTrait;
use crate::AppState;

pub async fn get_pokemon_external_info(Path(name): Path<String>) -> impl IntoResponse {
    let url = format!("https://pokeapi.co/api/v2/pokemon/{}", name);

    match reqwest::get(&url).await {
        Ok(response) => {
            match response.json::<PokemonExternalInfo>().await {
                Ok(pokemon) => {
                    let abilities = pokemon.abilities.into_iter().map(|a| a.ability.name).collect();
                    let moves = pokemon.moves.into_iter().map(|m| m.move_.name).collect();
                    let sprite_url = pokemon.sprites.front_default;

                    let transformed_pokemon = PokemonInfo {
                        abilities,
                        moves,
                        sprite_url,
                    };

                    (StatusCode::OK, Json(transformed_pokemon)).into_response()
                }
                Err(_) => (StatusCode::INTERNAL_SERVER_ERROR, "Failed to parse JSON").into_response(),
            }
        }
        Err(_) => (StatusCode::INTERNAL_SERVER_ERROR, "Request failed").into_response(),
    }
}

pub async fn get_created_pokemons(State(state): State<AppState>) -> impl IntoResponse {
    let pokemons = Pokemon::find().find_also_related(Pokedex).all(&state.db).await.unwrap();

    let pokemon_map: Vec<PokemonMapper> = pokemons.into_iter().map(|(pokemon, pokedex)| PokemonMapper {
        id: pokemon.id,
        name: pokedex.unwrap().nom_francais,
        ability: pokemon.ability,
        move1: pokemon.move1,
        move2: pokemon.move2,
        move3: pokemon.move3,
        move4: pokemon.move4,
        lien_image: pokemon.lien_image,
    }).collect();

    (StatusCode::OK, Json(pokemon_map)).into_response()
}

pub async fn get_pokedex_names(State(state): State<AppState>) -> impl IntoResponse {
    let pokedex = Pokedex::find()
        .into_json().all(&state.db).await.unwrap();

    (StatusCode::OK, Json(pokedex)).into_response()
}