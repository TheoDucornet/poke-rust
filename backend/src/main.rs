mod handlers;
mod models;

use axum::Router;
use axum::routing::{delete, get, post};
use handlers::{get_pokemon_external_info, create_new_pokemon, get_created_pokemons, get_pokedex_names,delete_pokemon};
use sea_orm::{Database, DatabaseConnection};
use tower_http::cors::CorsLayer;

#[derive(Clone)]
struct AppState {
    db: DatabaseConnection,
}

#[tokio::main]
async fn main() {
    dotenv::dotenv().ok();
    let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let db = Database::connect(&database_url).await.expect("Failed to connect to database");

    let app_state = AppState { db };

    let api_routes = Router::new()
        .route("/info/:pokemon", get(get_pokemon_external_info))
        .route("/pokemon/new", post(create_new_pokemon))
        .route("/pokemon", get(get_created_pokemons))
        .route("/pokedex", get(get_pokedex_names))
        .route("/pokemon/:id",delete(delete_pokemon))
        .layer(CorsLayer::permissive())
        .with_state(app_state);

    let app = Router::new()
        .nest("/api/v1", api_routes);

    let listener = tokio::net::TcpListener::bind("0.0.0.0:3000").await.unwrap();
    axum::serve(listener, app).await.unwrap();
}