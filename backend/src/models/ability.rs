use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct AbilityWrapper {
    pub ability: Ability,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Ability {
    pub name: String,
}