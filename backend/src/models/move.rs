use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct MoveWrapper {
    #[serde(rename = "move")]
    pub move_: Move,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Move {
    pub name: String,
}