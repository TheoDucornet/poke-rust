use serde::{Deserialize, Serialize};
use crate::models::{AbilityWrapper, MoveWrapper, Sprite};

#[derive(Debug, Serialize, Deserialize)]
pub struct PokemonExternalInfo {
    pub abilities: Vec<AbilityWrapper>,
    pub moves: Vec<MoveWrapper>,
    pub sprites: Sprite,
}