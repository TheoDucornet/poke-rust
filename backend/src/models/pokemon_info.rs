use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct PokemonInfo {
    pub abilities: Vec<String>,
    pub moves: Vec<String>,
    pub sprite_url: String,
}