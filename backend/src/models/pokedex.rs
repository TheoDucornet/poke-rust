use sea_orm::entity::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialEq, Eq, Deserialize, Serialize, DeriveEntityModel)]
#[sea_orm(table_name = "pokedex")]
pub struct Model {
    #[sea_orm(primary_key)]
    pub id: i32,
    pub nom_anglais: String,
    pub nom_francais: String,
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(has_many = "super::pokemon::Entity")]
    Pokemon,
}

impl Related<super::pokemon::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::Pokemon.def()
    }
}

impl ActiveModelBehavior for ActiveModel {}