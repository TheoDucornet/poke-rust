use sea_orm::entity::prelude::*;
use serde::{Deserialize, Serialize};


#[derive(Debug, Serialize, Deserialize)]
pub struct PokemonMapper {
    pub id: i32,
    pub name: String,
    pub ability: String,
    pub move1: String,
    pub move2: String,
    pub move3: String,
    pub move4: String,
    pub lien_image: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PokemonCreate {
    pub pokemon_id: i32,
    pub ability: String,
    pub move1: String,
    pub move2: String,
    pub move3: String,
    pub move4: String,
    pub lien_image: String,
}


#[derive(Clone, Debug, PartialEq, Eq, Deserialize, Serialize, DeriveEntityModel)]
#[sea_orm(table_name = "pokemon")]
pub struct Model {
    #[sea_orm(primary_key)]
    #[serde(skip_deserializing)]
    pub id: i32,
    pub pokemon_id: i32,
    pub ability: String,
    pub move1: String,
    pub move2: String,
    pub move3: String,
    pub move4: String,
    pub lien_image: String,
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(
        belongs_to = "super::pokedex::Entity",
        from = "Column::PokemonId",
        to = "super::pokedex::Column::Id"
    )]
    Pokedex,
}

impl Related<super::pokedex::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::Pokedex.def()
    }
}

impl ActiveModelBehavior for ActiveModel {}