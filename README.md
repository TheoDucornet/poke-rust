# poke rust

Front = React JS pour créer compte, connexion, création pokémons, voir ses pokémons

Backend Rust avec routes:

-   /api/v1/info/{nom} -> envoie le nom d'un pokémon et appel à l'API externe pour avoir ses moves possibles
-   /api/v1/pokemon/new -> prends un body {id_name, move1, move2, move3, move4, ability, item} et crée un pokémon en base
-   /api/v1/pokemon -> renvoie les pokémons
-   /api/v1/pokedex -> renvoie les nom fr des pokémons connus dans nos tables
-   /api/v1/pokemon/{id} -> delete un pokémon par son id

#### Table : pokemon

| Id (pk) | Name (FK pokedex) | Ability | Move 1 | Move 2 | Move 3 | Move 4 | lien_image |
| :------ | :---------------- | :------ | :----- | :----- | :----- | :----- | :--------- |

#### Table : pokedex

| Nom Anglais | Nom Français |
| :---------- | ------------ |

API Externe:
[PokeAPI](https://pokeapi.co/docs/v2#pokemon-section)

BDD Docker PostGre

Technos Rust: SQLX, Sea-ORM, Axum[https://github.com/tokio-rs/axum]

Faire appels propres avec Axum : https://docs.rs/axum/latest/axum/response/trait.IntoResponse.html

## Lancement du projet

#### Lancement du back

Dans le dossier backend, un docker-compose est fourni pour créer la base de donnée sur un container PostgreSQL.

```
cd backend
docker-compose up
```

Une fois la bdd opérationelle, il suffit de lancer le projet

```
cargo run
```

#### Lancement du front

Requis : **node.js**

```
cd frontend
npm i
npm start
```

`npm start` prompte de changer de port si 3000 déjà utilisé, par le back rust par exemple. Dans ce cas, port 3001 par défaut.

CLEMENT Matthieu, OBRY Thomas, DUCORNET Theo
